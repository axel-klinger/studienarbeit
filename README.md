# Studienarbeit "Untersuchung eines Fassadenelements mit zweifarbiger Beschichtung auf mögliche solare Energiegewinne"

Nachbau eines Word-Dokuments von 1999 in Markdown und Asciidoc zum Testen neuer und offener Formate.

Die Arbeit ist nicht vollständig. Einige Grafiken und die Tabellen, sowie die Software Komponenten fehlen hier.

Die folgenden Dokumente werden generiert

* [Studienarbeit als Ebook](https://axel-klinger.gitlab.io/studienarbeit/studienarbeit.epub)
* [Studienarbeit als PDF](https://axel-klinger.gitlab.io/studienarbeit/studienarbeit.pdf)
* [Studienarbeit als HTML](https://axel-klinger.gitlab.io/studienarbeit/index.html)
* [Studienarbeit als LiaScript](https://liascript.github.io/course/?https://axel-klinger.gitlab.io/studienarbeit/studienarbeit.md)
